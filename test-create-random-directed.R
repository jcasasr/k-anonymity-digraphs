createBarabasiGraph <- function(n, power, m) {
  # create a power-law network
  g <- barabasi.game(n=n, power=power, m=m, directed=TRUE);
  # plot
  plot(degree(g), col="red");
  # plot de degree histogram
  #plotLogLogScaleHistogram(g);
  # save
  #name <- paste(path, "barabasi-n", n, "-p", power, "-m", m, ".gml", sep="");
  #write.graph(g, file=name, format="gml");
  
  return(g);
}

createERGraph <- function(n, p) {
  # create
  g <- erdos.renyi.game(n, p/n, type="gnp", directed=TRUE, loops=FALSE);
  #g2 <- erdos.renyi.game(10000, 30000, type="gnm");
  
  # name
  name <- paste("er-n", n, "-p", p, sep="");
  
  # plot
  png(file=paste("plots/", name, ".png", sep=""), width=560, height=480, units='px', pointsize=18);
  plot(degree.distribution(g, mode="in"), xlab="Degree", ylab="Frequency", pch=1, col=1, type="b");
  points(degree.distribution(g, mode="out"), pch=2, col=2, type="b");
  dev.off();
  
  # save
  write.graph(g, file=paste(path, name, ".gml", sep=""), format="gml");
  
  return(g);
}

# create random graphs
#g <- createBarabasiGraph(1000, 1);
#g <- createERGraph(100, 8);
