# libraries
library(igraph);
library(numbers);
library(plot3D);
library(plyr);
library(sdcMicro);

# functions
source('functions/computeMetricValues.R');
source('functions/functions.R');
source('functions/k-anonymity.R');
source('functions/load.R');
source('functions/metrics.R');
source('functions/plot.R');
# Microaggregation
# Deprecated
#source('MicroAggregation/UniVariateMicroaggregation.R');
# DGA
source('DGA/DGA.R');
source('DGA/Julian.R');
source('DGA/Jordi.R');
source('DGA/FasterMicroaggregation.R');
source('DGA/IndependentDegSeqAnonimization.R');
source('DGA/CompleteDegSeqAnonimization.R');
source('DGA/GraphReconstruction.R');
# ILP
# Deprecated
#source('ILP/ILP.R');
# utils
source('utils/log.R');
source('utils/utils.R');
source('utils/utils-filesystem.R');
source('utils/utils-matrix.R');
source('utils/utils-statistics.R');
# clustering
source('clustering/clustering.R');
source('clustering/precision.R');

### dataset params
# synthetics: er-n100-p8, er-n1000-p10, barabasi-n1000-p1-m5
# datasets: physicians, celegansneural, polblogs, ucsocial
# medium: p2p-Gnutella08, wiki-vote, adolescent, advogato, dblpcite, soc-Epinions1
# large: soc-pokec, dblp-2006
path <- "/Volumes/DATA/K-Anonymity-digraphs-datasets/";
datasetName <- "polblogs";
datasetExt <- "gml";
datasetType <- "gml";

# params
CSet <- paste("C", 1:10, sep="-");
IkSet <- paste("I", 1:10, 1:10, sep="-");
ISet <- paste("I", rep(1:10, each=10), 1:10, sep="-");
anonSet <- c(CSet, IkSet);
testSet <- 1:10;

if(datasetName == "polblogs") {
  anonSet <- c(CSet, ISet);
} else if(datasetName == "soc-pokec") {
  testSet <- c(10,20,50,100);
}

# GIL
metricSet <- c("AD", "D", "EI", "EA", "BC", "CCin", "CCout", "DCin", "DCout");

if(datasetName == "soc-Epinions1") {
  metricSet <- c("AD", "D", "EA", "CCin", "CCout", "DCin", "DCout"); # EI, BC
}

# SIL
clusteringSet <- c("infomap", "walktrap");
useDimensionalityReduction <- TRUE;
maxDim <- 3000;

# DEBUG params
stopIfNoSolution=TRUE;
stopIfError=TRUE;
ILPmethod <- "Julian";

# info
loginfo("***********");
loginfo("*** PARAMS:");
loginfo("***********");
loginfo("Dataset: %s", datasetName);

if(datasetName == "soc-pokec") {
  g0 <- barabasi.game(n=10, power=2, m=20, directed=TRUE);
} else {
  # original graph
  g0 <- loadDigraph(paste(path, datasetName, "/graphs/", datasetName, ".", datasetExt, sep=""), datasetType);
}