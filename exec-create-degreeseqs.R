source('globals.R');

# paths
pathDir <- paste(path, datasetName, "/degreeseqs/", sep="");
createPath(pathDir);

# Complete
for(k in 1:10) {
  filename <- paste(pathDir, datasetName, "-C-", k, ".csv", sep="");
  
  if(!file.exists(filename)) {
    ds <- DGA(g0, k, method="Complete");
    
    # Write CSV
    write.table(ds, file=filename, sep=",", row.names=FALSE, col.names=FALSE);
    
  } else {
    loginfo("Complet %s-anonymous degree sequence exists at %s", k, filename);
  }
}

# Independent k
for(k in 1:10) {
  filename <- paste(pathDir, datasetName, "-I-", k, "-", k, ".csv", sep="");
  
  if(!file.exists(filename)) {
    ds <- DGA(g0, c(k,k), method="Independent");
    
    # Write CSV
    write.table(ds, file=filename, sep=",", row.names=FALSE, col.names=FALSE);
    
  } else {
    loginfo("Independent (%s,%s)-anonymous degree sequence exists at %s", k, k, filename);
  }
}

# Independent (ki,ko)
for(ki in 1:10) {
  for(ko in 1:10) {
    filename <- paste(pathDir, datasetName, "-I-", ki, "-", ko, ".csv", sep="");
    
    if(!file.exists(filename)) {
      ds <- DGA(g0, c(ki,ko), method="Independent");
      
      # Write CSV
      write.table(ds, file=filename, sep=",", row.names=FALSE, col.names=FALSE);
      
    } else {
      loginfo("Independent (%s,%s)-anonymous degree sequence exists at %s", ki, ko, filename);
    }
  }
}
