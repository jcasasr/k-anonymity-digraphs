library(logging);

# Logging configuration
basicConfig(level='FINEST');
# Level = INFO | DEBUG
setLevel(getHandler('basic.stdout'), level='DEBUG'); 

# output file
outputFile <- paste("log", ".txt", sep="");
addHandler(writeToFile, file=outputFile, level='DEBUG');
# close
removeHandler(writeToFile);