asd <- function(odin, odout, k, method="add", print=FALSE) {
  # create H(k,n)
  Hin <- createHkn(odin, k, method, print);
  
  allPathsIn <- allPaths(Hin);
  print("allPathsIn:");
  print(allPathsIn);
  
  # create H(k,n)
  Hout <- createHkn(odout, k, method, print);
  
  allPathsOut <- allPaths(Hout);
  print("allPathsOut:");
  print(allPathsOut);
  
  return(0);
}

allPaths <- function(H) {
  M <- as_adjacency_matrix(H, attr="weight", sparse=FALSE);
  paths <- all_simple_paths(H, 1, vcount(H));
  
  ar <- array(data=NA, dim=c(2, length(paths)));
  for(i in 1:length(paths)) {
    # save path as string
    p <- paste(as.integer(paths[[i]]), collapse="-")
    ar[i, 1] <- p;
    # compute path's cost
    a <- as.integer(strsplit(p, "-")[[1]]);
    cost <- 0;
    for(j in 1:(length(a)-1)) {
      cost <- cost + as.integer(M[a[j], a[j+1]]); #H[a[j,j+1]] is not working
    }
    ar[i, 2] <- cost;
  }
  
  return(ar);
}