asd2 <- function(odin, odout, k, method="add", print=FALSE) {
  # create H(k,n)
  Hin <- createHkn(odin, k, method, print);
  
  allPathsIn <- allPaths(Hin);
  print("allPathsIn:");
  print(allPathsIn);
  
  # create H(k,n)
  Hout <- createHkn(odout, k, method, print);
  
  allPathsOut <- allPaths(Hout);
  print("allPathsOut:");
  print(allPathsOut);
  
  return(0);
}

create.all.paths <- function(degseq, k, method, print) {
  # create H(k,n)
  H <- createHkn(degseq, k, method, print);
  
  # create table
  n <- length(degseq);
  t <- array(data=NA, dim=c((n+1),k));
  
  # fill table
  m <- ecount(H);
  for(i in 1:m) {
    # for each edge
    vs <- ends(H, i); # similar "get.edge(H, i)"
    logdebug("Processing edge (%s, %s)", vs[1], vs[2]);
    score <- get.edge.attribute(H, "weight", i);
    
    # index position
    p <- (vs[1] - (vs[2] - 2*k));
    t[vs[2], p] <- as.integer(score);
  }
  
  print(t);
  
  asp <- compute.path.and.cost(t, k);
  
  return(t);
}

compute.path.and.cost <- function(t, k) {
  
}
