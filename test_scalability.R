source('globals.R');
source('DGA/IndependentDegSeqAnonimization_no_FMA.R');
source('DGA/CompleteDegSeqAnonimization_no_kmeans.R');

# params
path <- "/Volumes/DATA/K-Anonymity-digraphs-datasets-large/";
dataset <- "dblp-2006"
f_indep <- FALSE
f_complet <- TRUE

# load digraph
g0 <- loadDigraph(paste(path, dataset, "/graphs/", dataset, ".txt", sep=""), "edgelist");

# original in- and out-degree sequences
g0_in <- degree(g0, mode="in", loops=FALSE, normalized=FALSE);
g0_out <- degree(g0, mode="out", loops=FALSE, normalized=FALSE);

# calcular numero arcs de sequencies originals
g0_num = sum(g0_in);
loginfo("Original digraph = %s arcs", g0_num);

###############
# Independent #
###############

if(f_indep) {
  loginfo("*** Independent k-degree anonymity")
  
  # comprovacions per cada valor de k
  for(k in c(10, 20, 50, 100)) {
    loginfo("+++ k=%s", k);
    
    ### NO FMA
    csvfile <- paste(path, dataset, "/degreeseqs-um/", dataset, "-I-", k, "-", k, "-um.csv", sep="");
    seqs <- IndependentDegSeqAnonimization_no_FMA(g0_in, g0_out, c(k,k), preDegSeq=csvfile);
    num_no_fma <- sum(seqs[[1]]);
    
    ### FMA
    preDegSeq <- paste(path, dataset, "/degreeseqs/", dataset, "-I-", k, "-", k, ".csv", sep="");
    res <- read.csv(file=preDegSeq, header=FALSE, sep=",")
    num_fma <- sum(res[1, ]);
    
    loginfo("NO FMA:: %s arcs [%s arcs added, + %s percent]", num_no_fma, (num_no_fma - g0_num), ((num_no_fma - g0_num)/g0_num)*100);
    loginfo("   FMA:: %s arcs [%s arcs added, + %s percent]", num_fma, (num_fma - g0_num), ((num_fma - g0_num)/g0_num)*100);
  }
}

###########
# Complet #
###########

if(f_complet) {
  loginfo("*** Paired k-degree anonymity")
  
  # comprovacions per cada valor de k
  for(k in c(10, 20, 50, 100)) {
    loginfo("+++ k=%s", k);
    
    ### NO KMEANS
    seqs <- CompleteDegSeqAnonimization_no_kmeans(g0_in, g0_out, k);
    num_no_fma <- sum(seqs[[1]]);
    
    ### KMEANS
    preDegSeq <- paste(path, dataset, "/degreeseqs/", dataset, "-C-", k, ".csv", sep="");
    res <- read.csv(file=preDegSeq, header=FALSE, sep=",")
    num_fma <- sum(res[1, ]);
    
    loginfo("NO KMEANS:: %s arcs [%s arcs added, + %s percent]", num_no_fma, (num_no_fma - g0_num), ((num_no_fma - g0_num)/g0_num)*100);
    loginfo("   KMEANS:: %s arcs [%s arcs added, + %s percent]", num_fma, (num_fma - g0_num), ((num_fma - g0_num)/g0_num)*100);
  }
}
