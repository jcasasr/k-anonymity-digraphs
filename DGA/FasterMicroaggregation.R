
# d: degree sequence
FMA <- function(d, k) {
  loginfo("Using FMA (FastMicroAggregation)...");
  
  # od sorted degree sequence
  od <- sort(d);
  
  T <- table(d);
  d.degree <- as.integer(names(T));
  d.frequency <- unname(T);
  
  d.histogram <- cbind(d.degree, d.frequency);
  d.resum <- Resume(d.histogram, k);
  
  # microaggregation
  Partition.resum <- Micro(d.resum, k);
  d.resum.k <- unlist(applyGroupPartition(d.resum, Partition.resum));
  d.first <- od[1:(length(d) - length(d.resum.k))];
  k.anonym.d <- c(d.first, d.resum.k);
  
  # Comprovacio: k-value
  H <- table(k.anonym.d);
  loginfo("K value is %s", min(H));
  
  return(k.anonym.d);
}

#
Resume <- function(M, k) {
  i <- 1;
  d.resum <- c();
  
  while (M[i, 2] > k - 1) {
    if (i < length(M[, 2])) {
      i <- i + 1; 
    } else {
      break;
    }
  }
  
  index <- last(which(M[1:i,2] > (2*k-1)));
  i <- index;
  
  while (i <= length(M[,2])) {
    d.resum <- c(d.resum, rep(M[i,1], M[i,2]));
    i <- i + 1;
  }
  
  return(d.resum);
}