# DGA Algorithm
# g: igraph network
# k: k value (integer when Complete and (k1,k2) when Independent)
# method: 'Complete' or 'Idependent'
# onlyDegSeq: compute only the k-anonymous degree sequence (do not modify the structure of the graph)
DGA <- function(g, k, method, onlyDegSeq=TRUE) {
  # Start the clock!
  ptStart <- proc.time();
  
  # g have to be directed
  if(!is.directed(g)) {
    logerror("Graph is not directed!");
    stop("Graph is not directed!");
  } else {
    printInfo(g, datasetName, k);
  }
  
  ##########################################
  # Step 1. Degree sequences anonymization #
  ##########################################
  
  # degree sequence of G
  degSeq <- paste(path, datasetName, "/degreeseqs/", datasetName, "-P-um.csv", sep="");
  
  if(file.exists(degSeq)) {
    # load in- and out-degree sequences from CSV file
    res <- loadDegSeq(degSeq);
    din <- res[1][[1]];
    dout <- res[2][[1]];
  } else {
    # compute in- and out-degree sequences
    din <- degree(graph=g, mode="in");
    dout <- degree(graph=g, mode="out");
  }
  
  # anonymize the degree sequence
  if(method=="Complete") {
    ##########
    # Paired #
    ##########
    loginfo("Computing COMPLETE K-DEGREE ANONYMITY")
    seqs <- CompleteDegSeqAnonimization(din, dout, k);
    
  } else if(method=="Independent") {
    ###############
    # Independent #
    ###############
    loginfo("Computing INDEPENDENT (Kin,Kout)-DEGREE ANONYMITY");
    if(!is.vector(k)) {
      logerror("k must be a vector!");
      stop("k must be a vector!");
    }
    seqs <- IndependentDegSeqAnonimization(din, dout, k);
  } else {
    logerror("Method '%s' does not exist!");
    stop("Incorrect parameter!");
  }
  
  # TODO: if there is no degree sequence, just return NULL
  if(is.null(seqs)) {
    return(NULL);
  }
  
  dink <- seqs[[1]];
  doutk <- seqs[[2]];
  
  # print results
  printResults(vcount(g), ecount(g), k, method, dink, doutk);
  
  # Write degree sequence to CSV
  if(onlyDegSeq) {
    # print time
    printTime(proc.time(), ptStart);
    
    return(rbind(dink, doutk));
  }
  
  ##############################
  # Step 2. Graph modification #
  ##############################
  
  # modifiy original graph to anonymize it
  gk <- GraphReconstruction(g, dink, doutk, k);
  
  # print statistics
  if(is.object(gk)) {
    # show k-anonymity value
    getIndependentKAnonymityFromDigraph(gk);
    getCompleteKAnonymityFromDigraph(gk);
    
    # show results
    #edgeIntersection(g, gk);
    #printEdgesAdded(g, gk);
    printTime(proc.time(), ptStart);
  } else {
    if(stopIfNoSolution) {
      stop("SOLUTION WAS NOT FOUND!");
    }
  }
  
  return(gk);
}

printInfo <- function(g, datasetName, k) {
  # info
  loginfo("***********");
  loginfo("*** DGA ***");
  loginfo("***********");
  loginfo("Dataset: %s", datasetName);
  loginfo("Properties: n=%s, m=%s, indep-k=%s, complet-k=%s", vcount(g), ecount(g), getIndependentKAnonymityFromDigraph(g), getCompleteKAnonymityFromDigraph(g));
  loginfo("K value: %s", k);
}

printEdgesAdded <- function(g, gk) {
  added <- ecount(gk)-ecount(g);
  
  loginfo("Edges added: %s [%s %%]", added, added/ecount(g)*100);
}

printResults <- function(n, m, k, method, dink, doutk) {
  loginfo("*** Degree sequence anonymization summary ***");
  
  ####################
  ### VERIFY
  if(sum(dink) != sum(doutk)) {
    logerror("The number of edges is not the same! [%s != %s]", sum(dink), sum(doutk));
    if(stopIfError) { stop(); }
  }
  
  ####################
  ### PRINT 
  indep <- c(getIndependentKAnonymityFromDegreeSequence(dink, doutk, printInfo=TRUE));
  compl <- getCompleteKAnonymityFromDegreeSequence(dink, doutk, printInfo=TRUE);
  loginfo("Properties: m=[%s -> %s (+%s edges)], indep-k=%s, complet-k=%s", m, sum(dink), (sum(dink)-m), indep, compl);
  
  if(method == "Complete") {
    if(k <= compl) {
      loginfo("RESULT: OK");
    } else {
      logerror("RESULT: ERROR");
      if(stopIfError) { stop(); }
    }
  } else if(method == "Independent") {
    if(sum((k <= indep)==FALSE) == 0) {
      loginfo("RESULT: OK");
    } else {
      logerror("RESULT: ERROR");
      if(stopIfError) { stop(); }
    }
  }
}

loadDegSeq <- function(fileName) {
  loginfo("Loading degree sequences from file '%s'...", fileName);
  
  if(file.exists(fileName)) {
    conn <- file(fileName,open="r");
    linn <-readLines(conn);
    
    # read in-degree
    din <- processOrder(linn[1]);
    # read out-degree
    dout <- processOrder(linn[2]);
    
    close(conn);
    
    return(list(din, dout));
    
  } else {
    logerror("Error, file '%s' does not exist!", filename);
    return(NULL);
  }
}